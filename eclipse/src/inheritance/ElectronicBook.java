//Antonio Simonelli 1736100
package inheritance;

public class ElectronicBook extends Book {
	private double numberBytes;

	public ElectronicBook(String title, String author, double numberBytes) {
		super(title, author);
		this.numberBytes = numberBytes;
	}

	@Override
	public String toString() {
		return super.toString() + ", Number of bytes = " + this.numberBytes;
	}

}
