//Antonio Simonelli 1736100
package inheritance;

public class Book {
	protected String title;
	private String author;

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public Book(String title, String author) {
		this.title = title;
		this.author = author;
	}

	@Override
	public String toString() {
		return "Title = " + this.title + ", Author = " + this.author;
	}

}
