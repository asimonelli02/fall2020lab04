//Antonio Simonelli 1736100
package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book books[] = new Book[5];
		books[0] = new Book("Horton Hatches the Egg", "Dr. Seuss");
		books[1] = new ElectronicBook("Gerald McBoing-Boing", "Dr. Seuss", 1050);
		books[2] = new Book("How the Grinch Stole Christmas", "Dr. Seuss");
		books[3] = new ElectronicBook("The Cat in the Hat", "Dr. Seuss", 1600);
		books[4] = new ElectronicBook("Horton Hears a Who!", "Dr. Seuss", 1850);

		for (int i = 0; i < books.length; i++) {
			System.out.println(books[i]);
		}
	}

}
