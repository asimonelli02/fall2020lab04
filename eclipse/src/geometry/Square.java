//Antonio Simonelli 1736100
package geometry;

public class Square extends Rectangle {

	public Square(double sideLength) {
		super(sideLength, sideLength);
	}
}
