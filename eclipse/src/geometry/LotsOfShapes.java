//Antonio Simonelli 1736100
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape shapes[] = new Shape[5];
		shapes[0] = new Rectangle(2, 2);
		shapes[1] = new Rectangle(4, 6);
		shapes[2] = new Circle(2.5);
		shapes[3] = new Circle(8.6);
		shapes[4] = new Square(10);

		for (int i = 0; i < shapes.length; i++) {
			System.out.println(shapes[i].getArea());
			System.out.println(shapes[i].getPerimeter());
		}
	}

}
